package com.korbiak.model.ex1;

import java.io.Serializable;
import java.util.Arrays;

public class Ship implements Serializable {

    private Droid[] array;

    public Ship() {
        array = new Droid[5];
        for (int i = 0; i < 5; i++) {
            array[i] = new Droid("125-485-00" + i + 1);
        }
    }

    @Override
    public String toString() {
        return "Ship{" +
                "array=" + Arrays.toString(array) +
                "}\n";
    }
}
