package com.korbiak.model.ex1;

import java.io.*;

public class Ex1 {

    public String getEx1() {
        String answer = "";
        Ship ship = new Ship();
        File file = new File("Ship.txt");
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(file))) {
            objectOutputStream.writeObject(ship);
            answer += "Written: " + ship.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream(file))) {
            ship = (Ship) objectInputStream.readObject();
            answer += "Read: " + ship.toString();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return answer;
    }
}
