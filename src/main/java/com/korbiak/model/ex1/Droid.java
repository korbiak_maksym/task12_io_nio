package com.korbiak.model.ex1;

import java.io.Serializable;
import java.util.List;

public class Droid implements Serializable {
    private String number;
    private int power;
    private transient int cost;

    public Droid(String number) {
        this.number = number;
        this.power = 150;
        this.cost = 1500;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "number='" + number + '\'' +
                ", power=" + power +
                ", cost=" + cost +
                "}\n";
    }
}
