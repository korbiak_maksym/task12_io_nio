package com.korbiak.model;

import com.korbiak.model.ex1.Ex1;

import java.io.IOException;
import java.util.ArrayList;

public class Model {

    private Ex1 ex1;
    private Ex2 ex2;
    private Ex3 ex3;
    private Ex4 ex4;
    private Ex5 ex5;

    public Model() {
        ex1 = new Ex1();
        ex2 = new Ex2();
        ex3 = new Ex3();
        ex4 = new Ex4();
        ex5 = new Ex5();
    }

    public String getEx1() {
        return ex1.getEx1();
    }

    public String getEx2() {
        String answer;
        try {
            answer = ex2.getSimple();
            answer += ex2.getBuffer();
        } catch (IOException e) {
            e.printStackTrace();
            answer = "err";
        }
        return answer;
    }

    public String getEx3(String path) {
        String answer = "Comments of code:\n";
        ArrayList<String> listOfComments = (ArrayList<String>)
                ex3.getListOfComments(path);
        for (int i = 0; i < listOfComments.size(); i++) {
            answer += listOfComments.get(i);
        }
        return answer;
    }

    public String getEx4(String path) {
        return ex4.getEx4(path);
    }

    public String getEx5() {
        String answer = "";
        try {
            answer += "Read:" + ex5.read();
            answer += "\nWrote:" + ex5.write();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }
}
