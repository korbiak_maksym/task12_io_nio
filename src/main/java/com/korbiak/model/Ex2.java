package com.korbiak.model;

import java.io.*;

public class Ex2 {

    File file;

    public Ex2() {
        file = new File("D:\\EPUM\\task12_io_nio\\src\\main\\resources\\sample.txt");
    }

    public String getSimple() throws IOException {
        long start = System.currentTimeMillis();
        String answer = "";
        InputStream inputStream = new FileInputStream(file);
        int data = inputStream.read();
        while (data != -1) {
            data = inputStream.read();
        }
        inputStream.close();
        long elapsedTime = System.currentTimeMillis() - start;
        answer += "Simple input stream:" + elapsedTime / 1000F;
        return answer;
    }

    public String getBuffer() throws IOException {
        long start = System.currentTimeMillis();
        String answer = "";
        BufferedInputStream bufferedInputStream = new BufferedInputStream(
                new FileInputStream(file));
        int data = bufferedInputStream.read();
        while (data != -1) {
            data = bufferedInputStream.read();
        }
        bufferedInputStream.close();
        long elapsedTime = System.currentTimeMillis() - start;
        answer += "\nBuffered input stream:" + elapsedTime / 1000F;
        return answer;
    }

}
