package com.korbiak.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;

public class Ex5 {
    private RandomAccessFile file;
    private FileChannel channel;
    private ByteBuffer buf;
    private int bytesRead;
    private int bytesWrite;

    public Ex5() {
        try {
            file = new RandomAccessFile("D:\\EPUM\\task12_io_nio\\src\\main\\resources\\sample.txt", "rw");
            channel = file.getChannel();
            buf = buf.allocate(48);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int read() throws IOException {
        bytesRead = channel.read(buf);
        buf.flip();
        return bytesRead;
    }

    public int write() throws IOException {
        bytesWrite = channel.write(buf);
        return bytesWrite;
    }
}
