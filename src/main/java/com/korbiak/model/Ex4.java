package com.korbiak.model;

import java.io.File;

public class Ex4 {

    public String getEx4(String path) {
        StringBuilder answ = new StringBuilder();
        File file = new File(path);
        String[] listOfFile = file.list();
        assert listOfFile != null;
        for (int i = 0; i < listOfFile.length; i++) {
            answ.append(listOfFile[i]).append("\n");
        }
        return answ.toString();
    }
}
