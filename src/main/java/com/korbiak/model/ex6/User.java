package com.korbiak.model.ex6;

import com.korbiak.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class User {
    private static final int BUFFER_SIZE = 1024;

    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(User.class);
        Scanner scanner = new Scanner(System.in);
        logger.info("Starting MySelectorClientExample...");
        try {
            int port = 9999;
            InetAddress hostIP = InetAddress.getLocalHost();
            InetSocketAddress myAddress =
                    new InetSocketAddress(hostIP, port);
            SocketChannel myClient = SocketChannel.open(myAddress);

            logger.info(String.format("Trying to connect to %s:%d...",
                    myAddress.getHostName(), myAddress.getPort()));
            String message = "";
            while (true){
                logger.info("Print message:");
                message = scanner.nextLine();
                ByteBuffer myBuffer=ByteBuffer.allocate(BUFFER_SIZE);
                myBuffer.put(message.getBytes());
                myBuffer.flip();
                int bytesWritten = myClient.write(myBuffer);
                logger.info(String
                        .format("Sending Message...: %s\nbytesWritten...: %d",
                                message, bytesWritten));
            }

        } catch (IOException e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        }
    }

}