package com.korbiak.model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

//Write a program that reads a Java source-code file (you provide the file name on the command line)
// and displays all the comments.
// Do not use regular expression
public class Ex3 {

    private String code;
    private List<String> listOfComments;

    public Ex3() {
        listOfComments = new ArrayList<>();
    }

    public List<String> getListOfComments(String path) {
        File file = new File(path);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                code += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int flag = 0;
        String comment = "";
        for (int i = 0; i < code.length(); i++) {
            if (code.charAt(i) == '/') {
                if (flag == 1) flag = 2;
                else flag = 1;
            }
            if (code.charAt(i) == '\n') {
                flag = 0;
                listOfComments.add(comment);
                comment = "";
            }
            if (flag == 2) {
                comment += code.charAt(i + 1);
            }
        }

        return listOfComments;
    }
}
