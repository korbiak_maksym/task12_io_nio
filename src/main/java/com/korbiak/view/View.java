package com.korbiak.view;

import com.korbiak.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(View.class);


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Create Ship with Droids. Serialize and deserialize them. Use transient.");
        menu.put("2", "2 - Performance of usual and buffered reader.");
        menu.put("3", "3 - Comments of code.");
        menu.put("4", "4 - Contents of a specific directory.");
        menu.put("5", "5 - SomeBuffer class.");
        menu.put("Q", "Q - exit");
    }

    public View() {

        controller = new Controller();
        input = new Scanner(System.in);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getEx1);
        methodsMenu.put("2", this::getEx2);
        methodsMenu.put("3", this::getEx3);
        methodsMenu.put("4", this::getEx4);
        methodsMenu.put("5", this::getEx5);
    }

    private void getEx1() {
        logger.info(controller.getEx1());
    }

    private void getEx2() {
        logger.info(controller.getEx2());
    }

    private void getEx3() {
        logger.info("Set path:");
        String path = input.nextLine();
        logger.info(controller.getEx3(path));
    }

    private void getEx4() {
        logger.info("Set path to directory:");
        String p = input.nextLine();
        logger.info(controller.getEx4(p));
    }

    private void getEx5() {
        logger.info(controller.getEx5());
    }




    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).getCom();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }
}
