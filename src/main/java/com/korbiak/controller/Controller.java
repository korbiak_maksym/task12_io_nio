package com.korbiak.controller;

import com.korbiak.model.Model;

public class Controller {

    Model model;

    public Controller() {
        model = new Model();
    }

    public String getEx1() {
        return model.getEx1();
    }

    public String getEx2() {
        return model.getEx2();
    }

    public String getEx3(String p) {
        return model.getEx3(p);
    }

    public String getEx4(String p) {
        return model.getEx4(p);
    }

    public String getEx5() {
        return model.getEx5();
    }
}
